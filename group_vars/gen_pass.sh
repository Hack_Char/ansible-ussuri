#!/bin/bash

IN_FILE='all.yml.in'
OUT_FILE='all.yml'

if [ -f $OUT_FILE ]; then
	echo Error, already have all.yml
	exit -1
fi

cp $IN_FILE $OUT_FILE
for (( i=0; i<15; i++)); do
	sed -i -e "0,/--PASSWORD--/{s/--PASSWORD--/`pwgen -s 32 1`/}" $OUT_FILE
done
