# ansible-ussuri

## Background

This is a set of ansible roles to install Openstack Ussuri onto Centos 8 compute and controller. Also installs a fixed cinder zfs driver for an ubuntu based storage host.
It was developed using xUbuntu running libvirt with Centos 8 VMs for control (192.168.122.11) and compute (192.168.122.12). Storage is hosted from the baremetal OS which has a zfs pool.
OpenVSwitch is used for networking. Expects two interfaces: management and provider. Significant data will still move on the management interface including VM traffic on self hosted
networks before it is routed to the provvider on the controller (network service installed there).

## Issues

The standard install guide for OpenStack Ussuri isn't complete for Centos 8. A few of the gotchas are:

* Cinder backend for Glance has a problem. The commit used for the Centos 8 packages has a typo on line 576. (Next source repo commit fixes it.)
* Horizon policy json files are not included in the package. Possibly intend to generate them with a command, but copied in these scripts.
* spice-html5 package is not in EPEL 8 yet - files included here from EPEL 7
* Serial console doesn't work through Horizon when spice or novnc is enabled

## Notes

Many files should be edited to reflect your environment before running these plays.
Make sure to check:

* inventory
* run group_vars/gen_pass.sh to populate passwords
* group_vars/all.yml 
* ussuri.yml installs packages to compute and controller prefering to put things on the controller
  * Controller VM requries ~6GB RAM but ideally 8GB
  * Using 20GB VM space for controller works, but when uploading an image it is cached in /tmp - may want more space
