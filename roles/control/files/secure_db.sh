#!/bin/bash

if /usr/bin/mysqladmin -u root status &> /dev/null ; then
# root password not set
	echo -e "\n\n$1\n$1\n\n\n\n\n" | /usr/bin/mysql_secure_installation
	touch /etc/my.cnf.d/root_password_set
fi

