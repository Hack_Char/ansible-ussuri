---
# install control services

- name: Install needed control packages
  yum:
    name: 
      - mariadb
      - mariadb-server
      - python2-PyMySQL
      - rabbitmq-server
      - memcached
      - python3-memcached
      - etcd
      - python3-PyMySQL
    state: present

- name: Set up MariaDB
  template: 
    src: files/openstack.cnf
    dest: /etc/my.cnf.d/openstack.cnf
    owner: root
    group: root
    mode: '0644'

- name: Enable and turn on control services
  systemd:
    name: "{{ item }}"
    enabled: yes
    state: started
  loop:
    - mariadb
    - rabbitmq-server
    - memcached
    - etcd
 
- name: Secure database
  script: files/secure_db.sh {{ MARIADB_ROOT }}
  args:
    creates: /etc/my.cnf.d/root_password_set

- name: Check if RabbitMQ set up
  shell: 'rabbitmqctl list_users --quiet | grep -q openstack'
  failed_when: 0 == 1
  register: rabbitmq_check

- name: Set up RabbitMQ user
  command:
    cmd: rabbitmqctl add_user openstack {{ RABBIT_PASS }}
  when: rabbitmq_check.rc == 1

- name: Set up RabbitMQ
  command: 
    cmd: rabbitmqctl set_permissions openstack ".*" ".*" ".*"
  when: rabbitmq_check.rc == 1

- name: RabbitMQ only listens on the mgmt IP
  lineinfile:
    path: /etc/rabbitmq/rabbitmq.conf
    line: "listeners.tcp.only = {{ MGMT_IP }}:5672"

- name: Configure etcd
  lineinfile:
    path: /etc/etcd/etcd.conf
    regexp: "{{ item.0 }}"
    line: "{{ item.1 }}"
    backrefs: yes
  loop:
    - ['.*ETCD_LISTEN_PEER_URLS\s*=\s*"http://[0-9.a-zA-Z]+:([0-9]+)"', 'ETCD_LISTEN_PEER_URLS="http://{{MGMT_IP}}:\1"']
    - ['.*ETCD_LISTEN_CLIENT_URLS\s*=\s*"http://[0-9.a-zA-Z]+:([0-9]+)"', 'ETCD_LISTEN_CLIENT_URLS="http://{{MGMT_IP}}:\1"']
    - ['.*ETCD_NAME\s*=\s*"[0-9.a-zA-Z]+"', 'ETCD_NAME="{{CONTROLLER_HOST}}"']
    - ['.*ETCD_INITIAL_ADVERTISE_PEER_URLS\s*=\s*"http://[0-9.a-zA-Z]+:([0-9]+)"', 'ETCD_INITIAL_ADVERTISE_PEER_URLS="http://{{MGMT_IP}}:\1"']
    - ['.*ETCD_ADVERTISE_CLIENT_URLS\s*=\s*"http://[0-9.a-zA-Z]+:([0-9]+)"', 'ETCD_ADVERTISE_CLIENT_URLS="http://{{MGMT_IP}}:\1"']
    - ['.*ETCD_INITIAL_CLUSTER\s*=\s*"[a-zA-Z0-9.]+=http://[0-9.a-zA-Z]+:([0-9]+)"', 'ETCD_INITIAL_CLUSTER="{{CONTROLLER_HOST}}=http://{{MGMT_IP}}:\1"']
    - ['.*ETCD_INITIAL_CLUSTER_TOKEN.*', 'ETCD_INITIAL_CLUSTER_TOKEN="etcd-cluster-01"']
    - ['.*ETCD_INITIAL_CLUSTER_STATE.*', 'ETCD_INITIAL_CLUSTER_STATE="new"']

- name: Copy needed selinux policies
  copy:
    src: files/ussuri_control.te
    dest: /root

- name: Compile and install selinux policies
  shell:
    cmd: "{{ item }}"
  loop:
    - checkmodule -M -m -o ussuri_control.mod ussuri_control.te
    - semodule_package -o ussuri_control.pp -m ussuri_control.mod
    - semodule -i ussuri_control.pp 

- name: populate the admin creds shell script
  template:
    src: files/admin.sh
    dest: /root
