---
# install cinder

- name: Install Cinder DB
  mysql_db:
    name: cinder
    state: present
    login_user: "root"
    login_password: "{{ MARIADB_ROOT }}"
    login_unix_socket: "/var/lib/mysql/mysql.sock"

- name: Add cinder DB user
  mysql_user:
    name: cinder
    host: '%'
    password: "{{CINDER_DB_PASS}}"
    encrypted: no
    state: present
    login_user: "root"
    login_password: "{{ MARIADB_ROOT }}"
    login_unix_socket: "/var/lib/mysql/mysql.sock"

- name: Add cinder DB user priv
  mysql_user:
    name: cinder
    host: '%'
    priv: 'cinder.*:ALL'
    state: present
    login_user: "root"
    login_password: "{{ MARIADB_ROOT }}"
    login_unix_socket: "/var/lib/mysql/mysql.sock"

- name: Create cinder OS user
  os_user:
    auth: "{{ OS_AUTH }}"
    state: present
    name: cinder
    password: "{{ CINDER_PASS }}"
    domain: default
    default_project: service

- name: Make cinder OS user and admin
  os_user_role:
    auth: "{{ OS_AUTH }}"
    user: cinder
    role: admin
    project: service

- name: Make the cinder service
  os_keystone_service:
    auth: "{{ OS_AUTH }}"
    state: present
    name: "{{ item.0 }}"
    service_type: "{{ item.1 }}"
    description: "OpenStack Block Storage"
  loop:
    - ['cinderv2','volumev2']
    - ['cinderv3','volumev3']

- name: Create API endpoints for glance
  os_keystone_endpoint:
    auth: "{{ OS_AUTH }}"
    service: "{{ item.0 }}"
    endpoint_interface: "{{ item.1 }}"
    url: "http://{{CONTROLLER_HOST}}:8776/{{ item.2 }}/%(project_id)s"
    region: "{{DEFAULT_REGION}}"
    state: present
  loop:
    - [ 'cinderv2', 'public', 'v2']
    - [ 'cinderv2', 'internal', 'v2']
    - [ 'cinderv2', 'admin', 'v2']
    - [ 'cinderv3', 'public', 'v3']
    - [ 'cinderv3', 'internal', 'v3']
    - [ 'cinderv3', 'admin', 'v3']

- name: Install needed cinder control packages
  yum:
    name:
      - openstack-cinder
    state: present

- name: Configure Cinder
  ini_file:
    path: /etc/cinder/cinder.conf
    section: "{{ item.0 }}"
    option: "{{ item.1 }}"
    value: "{{ item.2 }}"
  loop:
    - [ 'database', 'connection', "mysql+pymysql://cinder:{{CINDER_DB_PASS}}@{{CONTROLLER_HOST}}/cinder"]
    - [ 'DEFAULT', 'transport_url', "rabbit://openstack:{{ RABBIT_PASS }}@{{MGMT_IP}}"]
    - [ 'DEFAULT', 'auth_strategy', 'keystone']
    - [ 'DEFAULT', 'my_ip', "{{ MGMT_IP }}" ]
    - [ 'DEFAULT', 'image_upload_use_cinder_backend', 'true']
    - [ 'backend_defaults', 'image_upload_use_cinder_backend', 'true']
    - [ 'keystone_authtoken', 'www_authenticate_uri', "http://{{CONTROLLER_HOST}}:5000"]
    - [ 'keystone_authtoken', 'auth_url', "http://{{CONTROLLER_HOST}}:5000"]
    - [ 'keystone_authtoken', 'memcached_servers', "{{CONTROLLER_HOST}}:11211"]
    - [ 'keystone_authtoken', 'auth_type', 'password']
    - [ 'keystone_authtoken', 'project_domain_name', 'Default']
    - [ 'keystone_authtoken', 'user_domain_name', 'Default']
    - [ 'keystone_authtoken', 'project_name', 'service']
    - [ 'keystone_authtoken', 'username', 'cinder']
    - [ 'keystone_authtoken', 'password', "{{CINDER_PASS}}"]
    - [ 'oslo_concurrency', 'lock_path', '/var/lib/cinder/tmp']

- name: Populate Cinder DB
  shell: "/usr/bin/cinder-manage db sync"
  become: yes
  become_user: cinder

- name: Configure Nova for Cinder
  ini_file:
    path: /etc/nova/nova.conf
    section: "{{ item.0 }}"
    option: "{{ item.1 }}"
    value: "{{ item.2 }}"
  loop:
    - [ 'cinder', 'os_region_name', "{{DEFAULT_REGION}}"]

- name: Enable Cinder api
  systemd:
    name: "{{ item }}"
    enabled: yes
    state: started
  loop:
    - openstack-cinder-api.service
    - openstack-cinder-scheduler.service

- name: Restart nova api
  systemd:
    name: openstack-nova-api.service
    enabled: yes
    state: restarted



