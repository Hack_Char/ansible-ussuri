#!/bin/bash

# parameters for the provider network
START_IP_ADDRESS=192.168.122.100
END_IP_ADDRESS=192.168.122.254
DNS_RESOLVER=192.168.122.1
PROVIDER_NETWORK_GATEWAY=192.168.122.1
PROVIDER_NETWORK_CIDR=192.168.122.0/24
# parameters for the self service network
START_SELF_IP_ADDRESS=10.10.10.10
END_SELF_IP_ADDRESS=10.10.10.254
SELF_NETWORK_CIDR=10.10.10.0/24

source ~/admin.sh

# create initial provider network
openstack network create  --share --external --provider-physical-network provider --provider-network-type flat provider
openstack subnet create --network provider --allocation-pool start=${START_IP_ADDRESS},end=${END_IP_ADDRESS} --dns-nameserver ${DNS_RESOLVER} --gateway ${PROVIDER_NETWORK_GATEWAY} --subnet-range ${PROVIDER_NETWORK_CIDR} provider

# create initial self service network
openstack network create  self-service
openstack subnet create --network self-service --allocation-pool start=${START_SELF_IP_ADDRESS},end=${END_SELF_IP_ADDRESS} --dns-nameserver ${DNS_RESOLVER} --subnet-range ${SELF_NETWORK_CIDR} self-service

# add a router between self and provider
openstack router create router
openstack router add subnet router self-service
openstack router set router --external-gateway provider

# create default flavors
openstack flavor create --id 0 --vcpus 1 --ram 64    --disk 1   m1.nano
openstack flavor create --id 1 --vcpus 1 --ram 512   --disk 5   m1.tiny
openstack flavor create --id 2 --vcpus 1 --ram 1048  --disk 20  m1.small
openstack flavor create --id 3 --vcpus 2 --ram 2048  --disk 40  m1.medium
openstack flavor create --id 4 --vcpus 4 --ram 4096  --disk 80  m1.large
openstack flavor create --id 5 --vcpus 4 --ram 8192  --disk 100 m1.xlarge
openstack flavor create --id 6 --vcpus 8 --ram 16384 --disk 100 m1.xxlarge

# remove quota for volumes
openstack quota set --volumes -1 admin

